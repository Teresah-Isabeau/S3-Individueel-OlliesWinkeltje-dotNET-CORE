﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharp.Models;

namespace CSharp.Hubs
{
    public interface ChatClient
    {
        Task ReceiveMessage(ChatMessage message);
    }
}
