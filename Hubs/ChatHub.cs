﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using CSharp.Models;

namespace CSharp.Hubs
{
    public class ChatHub : Hub<ChatClient>
    {
        public async Task SendMessage(ChatMessage message)              
        {
            await Clients.All.ReceiveMessage(message);         
        }  
    }
}
