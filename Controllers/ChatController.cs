﻿using Microsoft.AspNetCore.Mvc;
using CSharp.Models;
using CSharp.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace CSharp.Controllers
{
    [Route("api/chat")]
    [ApiController]
    public class ChatController : ControllerBase
    {
        private readonly IHubContext<ChatHub> _hubContext;

        public ChatController(IHubContext<ChatHub> hubContext)
        {
            _hubContext = hubContext;
        }

        [Route("/send")]
        [HttpPost]
        public IActionResult SendRequest([FromBody] ChatMessage msg)
        {
            _hubContext.Clients.All.SendAsync("ReceiveOne", msg.User, msg.Message);
            return Ok();
        }
    }  
}
