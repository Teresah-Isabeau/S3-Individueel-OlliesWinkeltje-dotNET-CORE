﻿using CSharp.Models;
using CSharp.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CSharp.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private DataContext DataContext;
        public ProductController(DataContext context)
        {
            DataContext = context;
        }

        [HttpPost]
        public IActionResult CreateProduct([FromBody] ProductArgs args)
        {
            EntityEntry product = DataContext.Product.Add(new Product
            {
                Name = args.Name,
                Productcode = args.Productcode,
            });

            DataContext.SaveChanges();

            return Ok();
        }
    }
}
