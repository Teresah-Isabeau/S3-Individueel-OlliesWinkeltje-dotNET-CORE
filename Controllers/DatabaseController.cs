﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using CSharp.Models;


namespace CSharp.Controllers
{
    public class DatabaseController
    {    
        [ApiController]
        [Route("[controller]")]
        public class UserGroupController : ControllerBase
        {
            private DataContext myDbContext;

            public UserGroupController(DataContext context)
            {
                myDbContext = context;
            }

            [HttpGet]
            public IList<User> Get()
            {
                return (this.myDbContext.User.ToList());
            }
        }
    }
}
