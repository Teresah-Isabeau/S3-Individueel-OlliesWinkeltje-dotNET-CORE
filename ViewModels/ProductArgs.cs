﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSharp.ViewModels
{
    public class ProductArgs
    {
        //viewmodel om Json te vertalen naar Course Model
        public int? Id { get; set; }
        public string Name { get; set; }
        public int Productcode { get; set; }
        public ProductArgs()
        {

        }
        public ProductArgs(string name, int productcode)
        {
            Name = name;
            Productcode = productcode;
        }
        public ProductArgs(int id, string name, int productcode)
        {
            Id = id;
            Name = name;
            Productcode = productcode;
        }


    }
}
