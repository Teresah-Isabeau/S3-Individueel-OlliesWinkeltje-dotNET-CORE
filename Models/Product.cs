﻿namespace CSharp.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Product))]
    public class Product
    {
        [Key, Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Productcode { get; set; }
        public int Stock { get; set; }
        public virtual ICollection<Orderline> Orderlines { get; set; }
    }
}
