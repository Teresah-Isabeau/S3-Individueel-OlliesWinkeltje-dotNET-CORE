﻿namespace CSharp.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(nameof(Order))]
    public class Order
    {
        [Key, Required]
        public int Id { get; set; }

        [Required]
        public string Ordernumber { get; set; }
        public virtual User User { get; set; }
        public virtual List<Orderline> Orderlines { get; set; }

        public System.DateTime OrderDate { get; set; }
    }
}
