using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSharp.Models;

namespace CSharp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // TODO: UNCOMMENT
            //using (var context = new DataContext(options))
            //{

            //    var product = new Product() { Name = "Ollie", Productcode = 1297563, Stock = 500 };

            //    var user = new User() { Firstname = "Admin", Lastname = "Ollie", Email = "Admin@live.nl", Password = "adminOllie", Address = "Olliestraat 5", Zipcode = "1234AO", Country = "Netherlands"};
            //    var order = new Order() { Ordernumber = "ord_2021_001", User = user };
            //    var orderline = new Orderline() { Order = order, Product = product, Quantity = 2 };

            //    context.Product.Add(product);
            //    context.User.Add(user);
            //    context.Order.Add(order);
            //    context.Orderline.Add(orderline);
            //    context.SaveChanges();
            //}
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
